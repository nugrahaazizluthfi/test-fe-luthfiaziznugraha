import Vue from "vue";
import VueRouter from "vue-router";
import DefaultContainer from "@/layouts/default/DefaultContainer";

import Common from "@/router/common/index";
import Core from "@/router/admin/index";
import Assignment from "@/router/admin/assignment/index";
import Webinar from "@/router/admin/webinar/index";
import Video from "@/router/admin/video/index";
import Podcast from "@/router/admin/podcast/index";
import Files from "@/router/admin/files/index";
import Certificate from "@/router/admin/certificate/index";
import Course from "@/router/admin/course/index";
import Pathways from "@/router/admin/pathways/index";
import Voucher from "@/router/admin/voucher/index";
import Quiz from "@/router/admin/quiz/index";
import Category from "@/router/admin/category/index";
import Competencies from "@/router/admin/competencies/index";
import Account from "@/router/admin/account/index";
import Setting from "@/router/admin/setting/index";
import Analytic from "@/router/admin/analytic/index";
import Branding from "@/router/admin/branding/index";
import Integrasi from "@/router/admin/integrasi/index";

Vue.use(VueRouter);

const routes = [
  ...Common,
  { path: "/", redirect: "/admin/course/list", name: "home" },
  {
    path: "/admin",
    component: DefaultContainer,
    children: [
      ...Core,
      ...Assignment,
      ...Webinar,
      ...Video,
      ...Podcast,
      ...Files,
      ...Certificate,
      ...Course,
      ...Pathways,
      ...Voucher,
      ...Quiz,
      ...Category,
      ...Competencies,
      ...Account,
      ...Setting,
      ...Analytic,
      ...Branding,
      ...Integrasi,
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
