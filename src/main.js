import Vue from "vue";
import App from "@/layouts/default/App.vue";
import router from "./router";
import store from "./store";
import Antd from "ant-design-vue";
import PerfectScrollbar from "vue2-perfect-scrollbar";
import NProgress from "vue-nprogress";
import VueQuillEditor from "vue-quill-editor";
import VueHtml2Canvas from "vue-html2canvas";

import DefaultWrapper from "@/layouts/wrapper/DefaultWrapper";
import TableWrapper from "@/layouts/wrapper/TableWrapper";
import SizedBox from "@/components/SizedBox";
import BadgeBox from "@/components/BadgeBox";
import VAnimateCss from "v-animate-css";

import "ant-design-vue/dist/antd.css";
import "vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css";

import "quill/dist/quill.core.css"; // import styles
import "quill/dist/quill.snow.css"; // for snow theme
import "quill/dist/quill.bubble.css"; // for bubble theme

import VueFriendlyIframe from "vue-friendly-iframe";
import { mapState } from "vuex";

import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";

Vue.config.productionTip = false;
Vue.use(VueSweetalert2);
Vue.use(Antd);
Vue.use(PerfectScrollbar);
Vue.use(VueQuillEditor);
Vue.use(VueHtml2Canvas);
Vue.use(VueFriendlyIframe);
Vue.use(VAnimateCss);

Vue.component("main-wrapper", DefaultWrapper);
Vue.component("table-wrapper", TableWrapper);
Vue.component("sized-box", SizedBox);
Vue.component("badge-box", BadgeBox);

const options = {
  latencyThreshold: 200, // Number of ms before progressbar starts showing, default: 100,
  router: true, // Show progressbar when navigating routes, default: true
  http: false, // Show progressbar when doing Vue.http, default: true
};
Vue.use(NProgress, options);
const nprogress = new NProgress();

Vue.mixin({
  methods: {
    toRoute(name) {
      this.$router.push({ name });
    },
    notif({
      type = "success",
      message = "Notification Title",
      description = "This is the content of the notification.",
    }) {
      this.$notification[type]({
        message: message,
        description: description,
        duration: 2,
      });
    },
    resetForm(formName) {
      this.$refs[formName].resetFields();
    },
    formatRupiah(angka) {
      return new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
      }).format(angka);
    },
    g_message({
      type = "success",
      message = "Notification Title",
      description = "This is the content of the notification.",
    }) {
      this.$notification[type]({
        message: message,
        description: description,
        duration: 2,
      });
    },
    onlySuperAdmin() {
      return this.roleStatus === "SUPERADMIN";
    },
    isSuperRole() {
      return (
        this.roleStatus === "SUPERADMIN" || this.roleStatus === "SUPERLEMBAGA"
      );
    },
    isLembagaRole() {
      return this.roleStatus === "LEMBAGA";
    },
    isCreator() {
      return this.roleStatus === "USERCREATOR";
    },
    isApprover() {
      return this.roleStatus === "USERAPPROVER";
    },
    lembagaStatus(value) {
      switch (value) {
        case 0:
          return "Belum Aktif";
        case 1:
          return "Aktif";
        default:
          return "Belum Aktif";
      }
    },
    userStatus(value) {
      switch (value) {
        case 0:
          return "Belum Aktif";
        case 1:
          return "Aktif";
        default:
          return "Belum Aktif";
      }
    },
    programStatus(value) {
      switch (value) {
        case 0:
          return {
            text: "DALAM PENGAJUAN",
            color: "#333",
            bg: "#fff",
          };
        case 1:
          return {
            text: "AKTIF",
            color: "#fff",
            bg: "#32a649",
          };
        case 2:
          return {
            text: "DITOLAK",
            color: "#fff",
            bg: "#a63232",
          };

        default:
          return {
            text: "DALAM PENGAJUAN",
            color: "#333",
            bg: "#fff",
          };
      }
    },
    randomid: (idLength) =>
      [...Array(idLength).keys()]
        .map((elem) => Math.random().toString(36).substr(2, 1))
        .join(""),
  },
  computed: {
    ...mapState("libs", {
      roleStatus: (state) => state.label.role || "",
      lembagaOptions: (state) => state.options.lembaga || [],
    }),
    ...mapState("auth", {
      usersData: (state) => state.usersData || null,
      avatarUrl: (state) => state.usersData.defaultPartner.url_img || "",
      lembagaDefault: (state) => state.usersData.defaultPartner.id || "",
    }),
  },
});

new Vue({
  nprogress,
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
