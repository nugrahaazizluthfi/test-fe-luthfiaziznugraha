// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Competencies",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Competencies",
    data: [
      {
        text: "Competencies",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/competencies/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
