import CompetenciesContainer from "@/views/admin/competencies/views/ContainerView";
import CompetenciesIndex from "@/views/admin/competencies/views/IndexView";

export default [
  {
    path: "competencies",
    name: "competencies-parent",
    component: CompetenciesContainer,
    children: [
      {
        path: "list",
        name: "competencies-index",
        component: CompetenciesIndex,
      },
    ],
  },
];
