const initialState = () => ({
  isBroken: false,
  layoutPadding: {
    left: "260px",
  },
});

export default initialState;
