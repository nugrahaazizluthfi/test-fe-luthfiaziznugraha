// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Webinar",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Assignment",
    data: [
      {
        text: "Assignment",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/webinar/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
