import axios from "axios";
import { API_DEFAULT_URL, API_CLIENT_KEY, API_SERVER_KEY } from "@/env/default";
import { AuthService } from "@/services/admin/auth.service";

export class Http {
  constructor(status) {
    this.isAuth = status && status.auth ? status.auth : false;
    this.instance = axios.create({
      headers: {
        "x-client-key": API_CLIENT_KEY,
        "x-serv-key": API_SERVER_KEY,
      },
      baseURL: API_DEFAULT_URL,
    });

    return this.init();
  }

  init() {
    if (this.isAuth) {
      this.instance.interceptors.request.use(
        (request) => {
          request.headers.authorization = `Bearer ${AuthService.getBearer()}`;
          // if access token expired and refreshToken is exist >> go to API and get new access token
          if (
            AuthService.isAccessTokenExpired() &&
            AuthService.hasRefreshToken()
          ) {
            return AuthService.debounceRefreshTokens()
              .then((response) => {
                const data = response.data.data;
                AuthService.setBearer(data.token);
                request.headers.authorization = `Bearer ${AuthService.getBearer()}`;
                return request;
              })
              .catch((error) => Promise.reject(error));
          } else {
            return request;
          }
        },
        (error) => {
          console.log(error);
          return Promise.reject(error);
        }
      );
    }

    return this.instance;
  }
}
