const actions = {
  setBreakPoint({ commit }, payload) {
    commit("LAYOUT_SETBREAKPOINT", payload);
  },
  setLayoutPadding({ commit }, payload) {
    commit("LAYOUT_SETPADDING", payload);
  },
};

export default actions;
