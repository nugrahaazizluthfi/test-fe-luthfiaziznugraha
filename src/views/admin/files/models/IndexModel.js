// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Files",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Files",
    data: [
      {
        text: "Files",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/files/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
