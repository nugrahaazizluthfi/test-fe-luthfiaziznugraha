import WebinarContainer from "@/views/admin/webinar/views/ContainerView";
import WebinarIndex from "@/views/admin/webinar/views/IndexView";

export default [
  {
    path: "webinar",
    name: "webinar-parent",
    component: WebinarContainer,
    children: [
      {
        path: "list",
        name: "webinar-index",
        component: WebinarIndex,
      },
    ],
  },
];
