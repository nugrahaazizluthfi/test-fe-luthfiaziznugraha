import PodcastContainer from "@/views/admin/podcast/views/ContainerView";
import PodcastIndex from "@/views/admin/podcast/views/IndexView";

export default [
  {
    path: "podcast",
    name: "podcast-parent",
    component: PodcastContainer,
    children: [
      {
        path: "list",
        name: "podcast-index",
        component: PodcastIndex,
      },
    ],
  },
];
