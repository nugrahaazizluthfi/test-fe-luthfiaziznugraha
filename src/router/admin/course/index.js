import CourseContainer from "@/views/admin/course/views/ContainerView";
import CourseIndex from "@/views/admin/course/views/IndexView";
import CourseForm from "@/views/admin/course/views/FormView";
import CourseFormUpdate from "@/views/admin/course/views/FormUpdateView";

export default [
  {
    path: "course",
    name: "course-parent",
    component: CourseContainer,
    children: [
      {
        path: "list",
        name: "course-index",
        component: CourseIndex,
      },
      {
        path: "add",
        name: "course-form",
        component: CourseForm,
      },
      {
        path: "edit/:id",
        name: "course-update",
        component: CourseFormUpdate,
      },
    ],
  },
];
