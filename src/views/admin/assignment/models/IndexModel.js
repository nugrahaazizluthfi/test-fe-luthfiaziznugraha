// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Assignment",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Assignment",
    data: [
      {
        text: "Assignment",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/assignment/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
