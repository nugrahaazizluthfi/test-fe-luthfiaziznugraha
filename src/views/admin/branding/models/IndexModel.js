// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Branding",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Branding",
    data: [
      {
        text: "Branding",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/branding/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
