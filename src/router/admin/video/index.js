import VideoContainer from "@/views/admin/video/views/ContainerView";
import VideoIndex from "@/views/admin/video/views/IndexView";

export default [
  {
    path: "video",
    name: "video-parent",
    component: VideoContainer,
    children: [
      {
        path: "list",
        name: "video-index",
        component: VideoIndex,
      },
    ],
  },
];
