// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Setting",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Setting",
    data: [
      {
        text: "Setting",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/setting/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
