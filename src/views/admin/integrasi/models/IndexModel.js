// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Integrasi",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Integrasi",
    data: [
      {
        text: "Integrasi",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/integrasi/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
