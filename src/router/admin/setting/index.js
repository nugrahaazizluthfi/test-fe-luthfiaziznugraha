import SettingContainer from "@/views/admin/setting/views/ContainerView";
import SettingIndex from "@/views/admin/setting/views/IndexView";

export default [
  {
    path: "setting",
    name: "setting-parent",
    component: SettingContainer,
    children: [
      {
        path: "list",
        name: "setting-index",
        component: SettingIndex,
      },
    ],
  },
];
