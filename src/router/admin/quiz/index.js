import QuizContainer from "@/views/admin/quiz/views/ContainerView";
import QuizIndex from "@/views/admin/quiz/views/IndexView";

export default [
  {
    path: "quiz",
    name: "quiz-parent",
    component: QuizContainer,
    children: [
      {
        path: "list",
        name: "quiz-index",
        component: QuizIndex,
      },
    ],
  },
];
