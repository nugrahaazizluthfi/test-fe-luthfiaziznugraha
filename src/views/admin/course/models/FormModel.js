// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Course",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Course",
    data: [
      {
        text: "Course",
        path: "/admin/course/list",
      },
      {
        text: "Form",
        path: "/admin/course/add",
      },
    ],
  },
};
// end of breadcrumbs properties

const FormData = {
  course_detail: {
    title: "",
    start_date: "",
    end_date: "",
    caption: "",
    thumbnail: "",
    description: "",
  },
  content_category: {
    level: "",
    categories: "",
    competencies: [],
    business_domain: [],
  },
  content_control: {
    privacy: 1,
    sequence: 1,
    certified: 1,
    status: "",
    organization: [],
    learning_hours: [],
  },
  radioStyle: {
    display: "block",
    height: "30px",
    lineHeight: "30px",
  },
};
export default {
  ...BreadCrumbs,
  ...FormData,
};
