import $store from "@/store/index";

export class LayoutService {
  static setBreakPoint(isBroken) {
    $store.commit("layouts/LAYOUT_SETBREAKPOINT", isBroken);
    this.toggleSidebar(isBroken);
  }

  static toggleSidebar(collapsed) {
    const { isBroken } = $store.state.layouts;

    let left = collapsed ? "0px" : "200px";
    if (isBroken) {
      left = "0px";
    }
    $store.commit("layouts/LAYOUT_SETPADDING", {
      left: left,
    });
  }
}
