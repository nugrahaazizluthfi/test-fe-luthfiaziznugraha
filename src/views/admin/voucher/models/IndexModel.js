// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Voucher",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Voucher",
    data: [
      {
        text: "Voucher",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/voucher/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
