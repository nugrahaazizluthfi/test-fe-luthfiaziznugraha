import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

import admin from "@/store/modules/admin/index";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    ...admin,
  },
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  plugins: [createPersistedState()],
});
