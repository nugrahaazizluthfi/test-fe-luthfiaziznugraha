import IntegrasiContainer from "@/views/admin/integrasi/views/ContainerView";
import IntegrasiIndex from "@/views/admin/integrasi/views/IndexView";

export default [
  {
    path: "integrasi",
    name: "integrasi-parent",
    component: IntegrasiContainer,
    children: [
      {
        path: "list",
        name: "integrasi-index",
        component: IntegrasiIndex,
      },
    ],
  },
];
