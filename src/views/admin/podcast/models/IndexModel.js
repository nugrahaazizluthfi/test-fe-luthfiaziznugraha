// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Podcast",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Podcast",
    data: [
      {
        text: "Podcast",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/podcast/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
