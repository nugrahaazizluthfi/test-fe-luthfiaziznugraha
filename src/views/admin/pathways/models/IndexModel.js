// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Pathways",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Pathways",
    data: [
      {
        text: "Pathways",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/pathways/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
