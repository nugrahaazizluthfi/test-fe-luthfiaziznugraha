// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Quiz",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Quiz",
    data: [
      {
        text: "Quiz",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/quiz/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
