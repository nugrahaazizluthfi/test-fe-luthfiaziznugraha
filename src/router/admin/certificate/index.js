import CertificateContainer from "@/views/admin/certificate/views/ContainerView";
import CertificateIndex from "@/views/admin/certificate/views/IndexView";

export default [
  {
    path: "certificate",
    name: "certificate-parent",
    component: CertificateContainer,
    children: [
      {
        path: "list",
        name: "certificate-index",
        component: CertificateIndex,
      },
    ],
  },
];
