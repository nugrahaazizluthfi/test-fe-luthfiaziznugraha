import BrandingContainer from "@/views/admin/branding/views/ContainerView";
import BrandingIndex from "@/views/admin/branding/views/IndexView";

export default [
  {
    path: "branding",
    name: "branding-parent",
    component: BrandingContainer,
    children: [
      {
        path: "list",
        name: "branding-index",
        component: BrandingIndex,
      },
    ],
  },
];
