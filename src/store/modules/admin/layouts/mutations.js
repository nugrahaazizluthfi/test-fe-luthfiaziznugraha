const mutations = {
  LAYOUT_SETBREAKPOINT(state, payload) {
    state.isBroken = payload;
  },
  LAYOUT_SETPADDING(state, payload) {
    state.layoutPadding = { ...payload };
  },
};

export default mutations;
