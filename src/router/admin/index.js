import DashboardView from "@/views/admin/DashboardView.vue";

export default [
  {
    path: "",
    name: "dashboard",
    component: DashboardView,
  },
];
