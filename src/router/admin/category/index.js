import CategoryContainer from "@/views/admin/category/views/ContainerView";
import CategoryIndex from "@/views/admin/category/views/IndexView";

export default [
  {
    path: "category",
    name: "category-parent",
    component: CategoryContainer,
    children: [
      {
        path: "list",
        name: "category-index",
        component: CategoryIndex,
      },
    ],
  },
];
