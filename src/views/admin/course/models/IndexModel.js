// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Course",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Course",
    data: [
      {
        text: "Course",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/course/list",
      },
    ],
  },
};
// end of breadcrumbs properties

// start of table properties
const TableColumns = {
  columns: [
    {
      title: "#",
      dataIndex: "id",
      width: "100px",
    },
    {
      title: "TITLE",
      dataIndex: "title",
      width: "400px",
    },
    {
      title: "ORGANIZATION",
      dataIndex: "organization",
    },
    {
      title: "STATUS",
      align: "center",
      scopedSlots: { customRender: "statusSlot" },
    },
    {
      title: "LAST UPDATE",
      dataIndex: "created_ad",
    },
    {
      title: "ACTION",
      key: "action",
      fixed: "right",
      align: "center",
      scopedSlots: { customRender: "action" },
    },
  ],
};

const TableData = {
  search: "",
  data: [
    {
      id: 1,
      title: "Learn from Spiderman 3",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 2,
      title: "Learn from Captain Marvel",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 3,
      title: "Learn from Hulk",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 4,
      title: "Learn from Superman",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 5,
      title: "Learn from Batman",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 6,
      title: "Learn from Wonder Woman",
      organization: "MYDIGILEARN",
      video_status: false,
      created_ad: "08/22/2022",
    },
    {
      id: 7,
      title: "Learn from Captain America",
      organization: "MYDIGILEARN",
      video_status: false,
      created_ad: "08/22/2022",
    },
    {
      id: 8,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: false,
      created_ad: "08/22/2022",
    },
    {
      id: 9,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: false,
      created_ad: "08/22/2022",
    },
    {
      id: 10,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: false,
      created_ad: "08/22/2022",
    },
    {
      id: 11,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 12,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 13,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 14,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 15,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 16,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 17,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 18,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 19,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 20,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 21,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 22,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 23,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 24,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 25,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 26,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 27,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 28,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 29,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
    {
      id: 30,
      title: "Video Title",
      organization: "MYDIGILEARN",
      video_status: true,
      created_ad: "08/22/2022",
    },
  ],
  filter: false,
};
// end of table properties

export default {
  ...BreadCrumbs,
  ...TableColumns,
  ...TableData,
};
