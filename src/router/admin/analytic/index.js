import AnalyticContainer from "@/views/admin/analytic/views/ContainerView";
import AnalyticIndex from "@/views/admin/analytic/views/IndexView";

export default [
  {
    path: "analytic",
    name: "analytic-parent",
    component: AnalyticContainer,
    children: [
      {
        path: "list",
        name: "analytic-index",
        component: AnalyticIndex,
      },
    ],
  },
];
