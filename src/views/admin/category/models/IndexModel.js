// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Category",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Category",
    data: [
      {
        text: "Category",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/category/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
