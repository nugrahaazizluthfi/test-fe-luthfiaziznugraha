import PageNotFound from "@/views/admin/NotfoundView.vue";

export default [{ path: "*", component: PageNotFound }];
