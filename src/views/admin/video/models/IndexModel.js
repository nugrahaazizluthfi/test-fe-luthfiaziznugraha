// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Video",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Video",
    data: [
      {
        text: "Video",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/video/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
