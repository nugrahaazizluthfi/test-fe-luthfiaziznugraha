// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Analytic",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Analytic",
    data: [
      {
        text: "Analytic",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/analytic/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
