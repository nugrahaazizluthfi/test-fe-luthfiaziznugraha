import AccountContainer from "@/views/admin/account/views/ContainerView";
import AccountIndex from "@/views/admin/account/views/IndexView";

export default [
  {
    path: "account",
    name: "account-parent",
    component: AccountContainer,
    children: [
      {
        path: "list",
        name: "account-index",
        component: AccountIndex,
      },
    ],
  },
];
