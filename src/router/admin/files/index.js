import FilesContainer from "@/views/admin/files/views/ContainerView";
import FilesIndex from "@/views/admin/files/views/IndexView";

export default [
  {
    path: "files",
    name: "files-parent",
    component: FilesContainer,
    children: [
      {
        path: "list",
        name: "files-index",
        component: FilesIndex,
      },
    ],
  },
];
