// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Certificate",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Certificate",
    data: [
      {
        text: "Certificate",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/certificate/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
