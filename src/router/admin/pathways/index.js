import PathwaysContainer from "@/views/admin/pathways/views/ContainerView";
import PathwaysIndex from "@/views/admin/pathways/views/IndexView";

export default [
  {
    path: "pathways",
    name: "pathways-parent",
    component: PathwaysContainer,
    children: [
      {
        path: "list",
        name: "pathways-index",
        component: PathwaysIndex,
      },
    ],
  },
];
