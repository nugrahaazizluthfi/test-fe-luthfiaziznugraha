// start of breadcrumbs properties
const BreadCrumbs = {
  page: "Account",
  breadcrumbs: {
    useCrumbs: true,
    maintitle: "Account",
    data: [
      {
        text: "Account",
        path: "/admin",
      },
      {
        text: "Data",
        path: "/admin/account/list",
      },
    ],
  },
};
// end of breadcrumbs properties

export default {
  ...BreadCrumbs,
};
