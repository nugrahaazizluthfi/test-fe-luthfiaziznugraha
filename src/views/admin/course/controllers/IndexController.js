import Compressor from "compressorjs";

const Controllers = {
  handleSubmit() {},
  handleChange({ fileList }) {
    fileList.map((item) => {
      const file = item.originFileObj;
      new Compressor(file, {
        quality: 0.6,
        width: 600,
        success(result) {
          item.originFileObj = new File([result], file.name, {
            type: file.type,
            lastModified: new Date().getTime(),
          });
        },
        error(err) {
          console.log("error", err.message);
          return null;
        },
      });
    });

    this.fileList = fileList;
  },
  handleSearch(values) {},
  showFilter() {
    this.$refs.courseFilter.showFilter(!this.$refs.courseFilter.isShowed);
  },
  deleteItem(id) {
    let self = this;
    this.$swal({
      title: "Are you sure?",
      text: "Yout want to delete this Course",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "Cancel",
      reverseButtons: false,
    }).then((result) => {
      if (result.isConfirmed) {
        const updatedData = self.data.filter((item) => item.id !== id);
        self.data = updatedData;
        this.$notification["success"]({
          message: "Hapus data berhasil",
          description: "data telah berhasil dihapus",
        });
      }
    });
  },
};

export default Controllers;
