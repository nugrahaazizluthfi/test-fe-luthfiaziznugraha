import AssignmentContainer from "@/views/admin/assignment/views/ContainerView";
import AssignmentIndex from "@/views/admin/assignment/views/IndexView";

export default [
  {
    path: "assignment",
    name: "assignment-parent",
    component: AssignmentContainer,
    children: [
      {
        path: "list",
        name: "assignment-index",
        component: AssignmentIndex,
      },
    ],
  },
];
