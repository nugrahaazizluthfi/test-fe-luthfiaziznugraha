import VoucherContainer from "@/views/admin/voucher/views/ContainerView";
import VoucherIndex from "@/views/admin/voucher/views/IndexView";

export default [
  {
    path: "voucher",
    name: "voucher-parent",
    component: VoucherContainer,
    children: [
      {
        path: "list",
        name: "voucher-index",
        component: VoucherIndex,
      },
    ],
  },
];
